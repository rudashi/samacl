<?php

namespace Totem\SamAcl\App\Middleware;

use Totem\SamCore\App\Traits\HasApi;

class PermissionMiddleware
{
    use HasApi;

    public function handle(\Illuminate\Http\Request $request, \Closure $next, $permissions)
    {
        if (auth()->guest() || !$request->user()->can(explode('|', $permissions))) {
            return $this->response($this->error(403, __('No access to perform an action.')));
        }

        return $next($request);
    }

}
