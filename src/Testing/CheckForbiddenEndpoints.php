<?php

namespace Totem\SamAcl\Testing;

trait CheckForbiddenEndpoints
{
    use AssertForbiddenCall;

    public function test_forbidden_endpoint_get_all(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint");
    }

    public function test_forbidden_endpoint_get_one(): void
    {
        $this->assertForbiddenCall('get', "/api/$this->endpoint/1");
    }

    public function test_forbidden_endpoint_post_store(): void
    {
        $this->assertForbiddenCall('post', "/api/$this->endpoint");
    }

    public function test_forbidden_endpoint_delete_destroy(): void
    {
        $this->assertForbiddenCall('delete', "/api/$this->endpoint/1");
    }

    public function test_forbidden_endpoint_put_replace(): void
    {
        $this->assertForbiddenCall('put', "/api/$this->endpoint/1");
    }

}
