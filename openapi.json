{
  "openapi": "3.0.3",
  "info": {
    "title": "SAM-ACL docs",
    "description": "This package manages user roles and permissions in SAM.",
    "version": "latest",
    "contact": {
      "name": "Borys Żmuda",
      "email": "rudashi@gmail.com"
    }
  },
  "servers": [
    {
      "url": "https://{environment}/api",
      "variables": {
        "environment": {
          "default": "sam.totem.com.pl",
          "enum": [
            "sam.totem.com.pl",
            "v.totem.com.pl"
          ]
        }
      }
    }
  ],
  "paths": {
    "/permissions": {
      "get": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.view",
        "description": "Get all permissions",
        "summary": "permissions.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/PermissionCollection"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          }
        }
      },
      "post": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.create",
        "description": "Add a new permission",
        "summary": "permissions.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/PermissionCreateRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PermissionResource"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "docs/permission.json#/components/responses/ForbiddenError"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/permissions/{id}": {
      "get": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.show",
        "description": "Find permission by ID",
        "summary": "permissions.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of permission to return",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/PermissionResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/PermissionNotFound"
          }
        }
      },
      "put": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.replace",
        "description": "Update an existing permission by ID",
        "summary": "permissions.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of permission to update",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/PermissionReplaceRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PermissionResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/PermissionNotFound"
          }
        }
      },
      "delete": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.destroy",
        "description": "Delete permission by ID",
        "summary": "permissions.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of permission to delete",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/PermissionResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/PermissionNotFound"
          }
        }
      }
    },
    "/permissions/grouped": {
      "get": {
        "tags": [
          "Permissions"
        ],
        "operationId": "permissions.indexGrouped",
        "description": "Get permissions grouped by slug index",
        "summary": "permissions.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "slug-group": {
                      "type": "array",
                      "items": {
                        "$ref": "docs/permission.json#/components/schemas/Permission"
                      }
                    }
                  }
                }
              }
            }
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          }
        }
      }
    },
    "/roles": {
      "get": {
        "tags": [
          "Roles"
        ],
        "operationId": "roles.view",
        "description": "Get all roles",
        "summary": "roles.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/RoleCollection"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          }
        }
      },
      "post": {
        "tags": [
          "Roles"
        ],
        "operationId": "roles.create",
        "description": "Add a new role",
        "summary": "roles.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/RoleCreateRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/RoleResource"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "docs/permission.json#/components/responses/ForbiddenError"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/roles/{id}": {
      "get": {
        "tags": [
          "Roles"
        ],
        "operationId": "roles.show",
        "description": "Find role by ID",
        "summary": "roles.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of role to return",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/RoleResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/RoleNotFound"
          }
        }
      },
      "put": {
        "tags": [
          "Roles"
        ],
        "operationId": "roles.replace",
        "description": "Update an existing role by ID",
        "summary": "roles.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of role to update",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/RoleReplaceRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/RoleResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/RoleNotFound"
          }
        }
      },
      "delete": {
        "tags": [
          "Roles"
        ],
        "operationId": "roles.destroy",
        "description": "Delete role by ID",
        "summary": "roles.modify",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "Id of role to delete",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/RoleResource"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "$ref": "https://bitbucket.org/rudashi/samadmin/raw/HEAD/docs/auth.json#/components/responses/UnauthorizedError"
          },
          "403": {
            "$ref": "https://bitbucket.org/rudashi/samacl/raw/HEAD/docs/permission.json#/components/responses/ForbiddenError"
          },
          "404": {
            "$ref": "#/components/responses/RoleNotFound"
          }
        }
      }
    }
  },
  "components": {
    "securitySchemes": {
      "bearer-token": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    },
    "requestBodies": {
      "PermissionCreateRequest": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "required": [
                "slug",
                "name"
              ],
              "properties": {
                "slug": {
                  "type": "string",
                  "example": "users.create"
                },
                "name": {
                  "type": "string",
                  "example": "Can Create Users"
                },
                "description": {
                  "type": "string",
                  "example": "Can create new users",
                  "nullable": true
                },
                "roles": {
                  "type": "array",
                  "items": {
                    "type": "integer"
                  },
                  "example": [1, 5],
                  "nullable": true
                }
              }
            }
          }
        }
      },
      "PermissionReplaceRequest": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "required": [
                "name"
              ],
              "properties": {
                "name": {
                  "type": "string",
                  "example": "Can Create Users"
                },
                "description": {
                  "type": "string",
                  "example": "Can create new users",
                  "nullable": true
                },
                "roles": {
                  "type": "array",
                  "items": {
                    "type": "integer"
                  },
                  "example": [1, 5],
                  "nullable": true
                }
              }
            }
          }
        }
      },
      "RoleCreateRequest": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "required": [
                "slug",
                "name",
                "permissions"
              ],
              "properties": {
                "slug": {
                  "type": "string",
                  "example": "admin"
                },
                "name": {
                  "type": "string",
                  "example": "Administrator"
                },
                "description": {
                  "type": "string",
                  "example": "Admin role",
                  "nullable": true
                },
                "permissions": {
                  "type": "array",
                  "items": {
                    "type": "integer"
                  },
                  "example": [1, 5]
                }
              }
            }
          }
        }
      },
      "RoleReplaceRequest": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "required": [
                "name",
                "permissions"
              ],
              "properties": {
                "name": {
                  "type": "string",
                  "example": "Administrator"
                },
                "description": {
                  "type": "string",
                  "example": "Admin role",
                  "nullable": true
                },
                "permissions": {
                  "type": "array",
                  "items": {
                    "type": "integer"
                  },
                  "example": [1, 5]
                }
              }
            }
          }
        }
      }
    },
    "responses": {
      "PermissionResource": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "docs/permission.json#/components/schemas/Permission"
            }
          }
        }
      },
      "PermissionCollection": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "type": "array",
              "items": {
                "$ref": "docs/permission.json#/components/schemas/Permission"
              }
            }
          }
        }
      },
      "PermissionNotFound": {
        "description": "Not Found",
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "code": {
                        "type": "integer",
                        "default": 404
                      },
                      "message": {
                        "type": "string",
                        "default": "Given id {id} is invalid or permission not exist."
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      "RoleResource": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "docs/permission.json#/components/schemas/Role"
            }
          }
        }
      },
      "RoleCollection": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "type": "array",
              "items": {
                "$ref": "docs/permission.json#/components/schemas/Role"
              }
            }
          }
        }
      },
      "RoleNotFound": {
        "description": "Not Found",
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "properties": {
                "data": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "code": {
                        "type": "integer",
                        "default": 404
                      },
                      "message": {
                        "type": "string",
                        "default": "Given id {id} is invalid or role not exist."
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

  }
}
