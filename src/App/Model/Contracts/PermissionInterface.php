<?php

namespace Totem\SamAcl\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Totem\SamAcl\App\Model\Role;

interface PermissionInterface
{
    /**
     * Permission belongs to many roles.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany;

    /**
     * Permission belongs to many users.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany;

    /**
     * Attach permission to a role.
     *
     * @param int|Role $role
     * @return void
     */
    public function attachRole($role): void;

    /**
     * Attach multiple roles to a permission.
     *
     * @param array $roles
     * @return void
     */
    public function attachRoles(array $roles): void;

    /**
     * Detach role from a permission.
     *
     * @param int|Role $role
     * @return int
     */
    public function detachRole($role): int;

    /**
     * Detach multiple roles from a permission
     *
     * @param array $roles
     * @return void
     */
    public function detachRoles(array $roles): void;

    /**
     * Sync roles for a permission.
     *
     * @param array|Role[]|\Illuminate\Support\Collection $roles
     * @return array
     */
    public function syncRoles($roles): array;

}
