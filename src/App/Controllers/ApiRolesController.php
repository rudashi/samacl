<?php

namespace Totem\SamAcl\App\Controllers;

use Totem\SamAcl\App\Repositories\Contracts\RoleRepositoryInterface;
use Totem\SamAcl\App\Requests\RoleRequest;
use Totem\SamAcl\App\Resources\RoleCollection;
use Totem\SamAcl\App\Resources\RoleResource;
use Totem\SamCore\App\Controllers\ApiController;

class ApiRolesController extends ApiController
{

    public function __construct(RoleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(RoleRequest $request): RoleResource
    {
        return new RoleResource($this->repository->store($request));
    }

    public function index(): RoleCollection
    {
        return new RoleCollection(
            $this->getFromRequestQuery($this->repository->all())
        );
    }

    public function show(int $id): RoleResource
    {
        return new RoleResource(
            $this->getFromRequestQuery($this->repository->findById($id))
        );
    }

    public function replace(int $id, RoleRequest $request): RoleResource
    {
        return new RoleResource($this->repository->store($request, $id));
    }

    public function destroy(int $id): RoleResource
    {
        return new RoleResource($this->repository->delete($id));
    }

}
