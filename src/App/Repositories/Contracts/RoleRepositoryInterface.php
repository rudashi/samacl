<?php

namespace Totem\SamAcl\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Totem\SamAcl\App\Model\Role;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface RoleRepositoryInterface extends RepositoryInterface
{

    public function allWithUsers(array $columns = ['*']): Collection;

    public function findById(int $id = 0, array $columns = ['*']): Role;

    public function store(Request $request, int $id = 0): Role;

}
