<?php

namespace Totem\SamAcl\Testing;

trait AssertForbiddenCall
{

    private function assertForbiddenCall(string $method, string $url): void
    {
        $this->withoutRole()->call($method, $url)
            ->assertForbidden()
            ->assertJsonFragment([
                'error' => [
                    'code' => 403,
                    'message' => __('No access to perform an action.'),
                ],
            ]);
    }

}
