<?php

namespace Totem\SamAcl\App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamUsers\App\Model\User;

/**
 * @property null|Collection|Permission[] permissions
 * @property null|Collection|User[] users
 */
trait RoleTrait
{

    protected ?Collection $cachedPermissions = null;

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    public function cachedPermissions(): \Illuminate\Support\Collection
    {
        return (!$this->cachedPermissions) ? $this->cachedPermissions = $this->permissions()->get() : $this->cachedPermissions;
    }


    public function attachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->attachPermission($permission);
        }
    }

    public function attachPermission($permission): void
    {
        $this->checkPermission($permission) ?: $this->permissions()->attach($permission);
    }

    public function checkPermission($permission): bool
    {
        return $this->cachedPermissions()->contains(static function ($value) use ($permission) {
            return $permission === $value->id || Str::is($permission, $value->slug);
        });
    }

    public function detachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->detachPermission($permission);
        }
    }

    public function detachPermission($permission): int
    {
        return $this->permissions()->detach($permission);
    }

    public function detachAllPermissions(): int
    {
        return $this->detachPermission(null);
    }

    public function syncPermissions($permissions): array
    {
        return $this->permissions()->sync($permissions);
    }

    public function hasPermission($permissions, $requireAll = false): bool
    {
        if (!$requireAll) {
            foreach ((array)$permissions as $permission) {
                if ($this->checkPermission($permission)) {
                    return true;
                }
            }
            return false;
        }
        return $this->hasAllPermissions($permissions);
    }

    public function hasAllPermissions(array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (!$this->checkPermission($permission)) {
                return false;
            }
        }
        return true;
    }

}
