<?php

namespace Tests;

use Totem\SamAcl\App\Model\Role;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class RoleApiTest extends ApiCrudTest
{

    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $model = Role::class;
    protected string $endpoint = 'roles';

    protected array $additionalFields = [
        'permissions' => [ 1, 2, 4, 6 ]
    ];

    protected function createModel(array $attributes = []) : Role
    {
        $model = factory($this->model)->create($attributes);
        $model->attachPermissions(Permission::all());
        $model->load('permissions');

        return $model;
    }

}
