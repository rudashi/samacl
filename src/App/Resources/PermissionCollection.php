<?php

namespace Totem\SamAcl\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class PermissionCollection extends ApiCollection
{

    public $collects = PermissionResource::class;

}
