<?php

namespace Totem\SamAcl\Database;

trait PermissionTraitSeeder
{

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    abstract public function permissions(): array;

    public function run(): void
    {
        /** @var \Totem\SamAcl\App\Model\Role|null $roleAdmin */
        /** @var \Totem\SamAcl\App\Model\Permission $permission */

        $roleAdmin = \Totem\SamAcl\App\Model\Role::query()->where('slug', 'admin')->first();

        foreach ($this->permissions() as $item) {

            $permission = \Totem\SamAcl\App\Model\Permission::query()->updateOrCreate([ 'slug' => $item['slug'] ], $item);

            if ($roleAdmin !== null && $roleAdmin->hasPermission($permission->name) === false) {
                $roleAdmin->attachPermission($permission);
            }
        }
    }

    /**
     * @param array|null $permissions
     * @throws \Exception
     */
    public function down(array $permissions = null): void
    {
        \Totem\SamAcl\App\Model\Permission::query()->whereIn('slug', array_column($permissions ?? $this->permissions(), 'slug'))->delete();
    }

}
