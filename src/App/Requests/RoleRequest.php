<?php

namespace Totem\SamAcl\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class RoleRequest extends BaseRequest
{

    public function rules(): array
    {
        return [
            'slug' => ($this->method() === 'POST' ? 'required|' : '') . 'unique:roles,slug,' . $this->id,
            'name' => 'required|unique:roles,name,' . $this->id,
            'description' => 'nullable',
            'permissions' => 'required|array',
        ];
    }

    public function attributes(): array
    {
        return [
            'slug' => __('Slug'),
            'name' => __('Name'),
            'description' => __('Description'),
            'permissions' => __('Permissions'),
        ];
    }
}
