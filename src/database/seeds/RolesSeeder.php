<?php

namespace Totem\SamAcl\Database\Seeds;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'slug' => 'admin',
                'name' => 'Administrator',
                'description' => 'Admin Role'
            ],
            [
                'slug' => 'user',
                'name' => 'User',
                'description' => 'User Role'
            ],
        ];

        \Totem\SamAcl\App\Model\Role::query()->insert($data);
    }

}
