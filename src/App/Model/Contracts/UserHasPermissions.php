<?php

namespace Totem\SamAcl\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Totem\SamAcl\App\Model\Permission;

interface UserHasPermissions
{

    /**
     * User belongs to many permissions.
     *
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany;

    /**
     * Check if the user has a permission or permissions.
     *
     * @param int|string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function hasPermission($permission, bool $requireAll = false): bool;

    /**
     * Check if the user has a permission or permissions by its name.
     *
     * @param int|string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function can($permission, bool $requireAll = false): bool;

    /**
     * Check if the user hasn't a permission or permissions by its name.
     *
     * @param int|string|array $permission
     * @return bool
     */
    public function cant($permission): bool;

    /**
     * Alias to can't method.
     *
     * @param int|string|array $permission
     * @return bool
     */
    public function cannot($permission): bool;

    /**
     * Check if the user has an individual permission or permissions by its name.
     *
     * @param int|string|array $permission
     * @return bool
     */
    public function able($permission): bool;

    /**
     * Attach permission to a user.
     *
     * @param int|Permission $permission
     * @return void
     */
    public function attachPermission($permission): void;

    /**
     * Attach multiple permissions to a user
     *
     * @param array $permissions
     * @return void
     */
    public function attachPermissions(array $permissions): void;

    /**
     * Detach permission from a user.
     *
     * @param int|Permission $permission
     * @return void
     */
    public function detachPermission($permission): void;

    /**
     * Detach multiple permissions from a user
     *
     * @param array $permissions
     * @return void
     */
    public function detachPermissions(array $permissions): void;

    /**
     * Sync permissions for a user.
     *
     * @param array|Permission[]|Collection $permissions
     * @return array
     */
    public function syncPermissions($permissions): array;

}
