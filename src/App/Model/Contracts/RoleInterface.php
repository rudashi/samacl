<?php

namespace Totem\SamAcl\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Totem\SamAcl\App\Model\Permission;

interface RoleInterface
{
    /**
     * Role belongs to many permissions.
     *
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany;

    /**
     * Role belongs to many users.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany;

    /**
     * Attach permission to a role.
     *
     * @param int|Permission $permission
     * @return void
     */
    public function attachPermission($permission): void;

    /**
     * Attach multiple permissions to a role.
     *
     * @param array $permissions
     * @return void
     */
    public function attachPermissions(array $permissions): void;

    /**
     * Detach permission from a role.
     *
     * @param int|Permission $permission
     * @return int
     */
    public function detachPermission($permission): int;

    /**
     * Detach multiple permissions from a role.
     *
     * @param array $permission
     * @return void
     */
    public function detachPermissions(array $permission): void;

    /**
     * Detach all permissions.
     *
     * @return int
     */
    public function detachAllPermissions(): int;

    /**
     * Sync permissions for a role.
     *
     * @param array|Permission[]|\Illuminate\Support\Collection $permissions
     * @return array
     */
    public function syncPermissions($permissions): array;

}
