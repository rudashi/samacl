<?php

namespace Totem\SamAcl\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class PermissionRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'slug' => ($this->method() === 'POST' ? 'required|' : '') . 'unique:permissions,slug,' . $this->id,
            'name' => 'required|unique:permissions,name,' . $this->id,
            'description' => 'nullable',
            'roles' => 'array|nullable'
        ];
    }

    public function attributes() : array
    {
        return [
            'slug' => __('Slug'),
            'name' => __('Name'),
            'description' => __('Description'),
            'roles' => __('Roles'),
        ];
    }
}
