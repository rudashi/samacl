<?php

namespace Totem\SamAcl\App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection as CollectionSupport;
use Illuminate\Support\Str;
use Totem\SamAcl\App\Model\Role;

/**
 * @property null|Collection|Role[] roles
 */
trait UserHasRolesTrait
{

    protected ?Collection $cachedRoles = null;

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public function cachedRoles(): Collection
    {
        if ($this->cachedRoles === null || $this->cachedRoles->isEmpty()) {
            if ($this->relationLoaded('roles')) {
                $this->cachedRoles = $this->getRelation('roles')->with('permissions')->get();
                return $this->cachedRoles;
            }
            $this->cachedRoles = $this->roles()->with('permissions')->get();
        }
        return $this->cachedRoles;
    }

    public function rolePermissions(): CollectionSupport
    {
        return $this->cachedRoles()->flatMap(static function (Role $role) {
            return $role->permissions;
        })->unique('slug');
    }

    public function hasRole($roles, bool $requireAll = false): bool
    {
        if ($requireAll === false) {
            foreach ((array)$roles as $role) {
                if ($this->checkRole($role)) {
                    return true;
                }
            }
            return false;
        }
        return $this->hasAllRoles($roles);
    }

    public function checkRole($role): bool
    {
        return $this->cachedRoles()->contains(static function ($value) use ($role) {
            return $role === $value->id || Str::is($role, $value->slug);
        });
    }

    public function hasAllRoles(array $roles): bool
    {
        foreach ($roles as $role) {
            if (!$this->checkRole($role)) {
                return false;
            }
        }
        return true;
    }

    public function attachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function attachRole($role): void
    {
        $this->checkRole($role) ?: $this->roles()->attach($role);
    }

    public function detachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function detachRole($role): int
    {
        return $this->roles()->detach($role);
    }

    public function syncRoles($roles): array
    {
        return $this->roles()->sync($roles);
    }

}
