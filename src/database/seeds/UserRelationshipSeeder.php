<?php

namespace Totem\SamAcl\Database\Seeds;

use Illuminate\Database\Seeder;

class UserRelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var null|\Totem\SamAcl\App\Model\Role $roleAdmin */
        /** @var null|\Totem\SamUsers\App\Model\User $userAdmin */
        $roleAdmin = \Totem\SamAcl\App\Model\Role::query()->where('slug', 'admin')->first();
        $userAdmin = \Totem\SamUsers\App\Model\User::query()->find(1);

        if ($roleAdmin !== null && $userAdmin !== null) {
            $userAdmin->attachRole($roleAdmin);
        }
    }

}
