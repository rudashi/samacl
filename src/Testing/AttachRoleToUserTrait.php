<?php

namespace Totem\SamAcl\Testing;

use Totem\SamAcl\App\Model\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property \Totem\SamAcl\App\Model\Contracts\UserHasRoles|JWTSubject loginUser
 */
trait AttachRoleToUserTrait
{

    protected bool $without_role = false;

    protected function withoutRole(): self
    {
        $this->without_role = true;

        return $this;
    }

    protected function setUser(): JWTSubject
    {
        parent::setUser();

        if (!$this->without_role) {
            /** @var Role $role*/
            $role = Role::query()->first();
            $this->loginUser->attachRole($role);
        }

        return $this->loginUser;
    }

}
