<?php

namespace Totem\SamAcl\App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;

/**
 * @property \Illuminate\Database\Eloquent\Builder|Permission model
 */
class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{

    public function model(): string
    {
        return Permission::class;
    }

    public function allGrouped(): Collection
    {
        return $this->all(['id', 'slug', 'name', 'description'])->groupBy(static function ($item) {
            if (strpos($item->slug, '.') === false) {
                return $item->slug;
            }
            return strstr($item->slug, '.', true);
        });
    }

    public function findById(int $id = 0, array $columns = ['*']): Permission
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']): Permission
    {
        if ($id === 0) {
            throw new RepositoryException(__('No permission id have been given.'));
        }
        /** @var Permission $data */
        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(__('Given id :code is invalid or permission not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function store(Request $request, int $id = 0): Permission
    {
        $permission = ($id === 0) ? $this->model : $this->find($id);

        if ($id === 0) {
            $permission->slug = $request->input('slug');
        }

        $permission->name = $request->input('name');
        $permission->description = $request->input('description');

        if ($id > 0 && $request->has('roles')) {
            $permission->syncRoles([]);
        }

        $permission->save();
        $permission->attachRoles($request->has('roles') ? $request->input('roles') : []);

        return $permission;
    }

    public function findBySlug(string $slug = null, array $relationships = [], array $columns = ['*']): Permission
    {
        if ($slug === null) {
            throw new RepositoryException(__('No permission id have been given.'));
        }

        /** @var Permission $data */
        $data = $this->model->with($relationships)->where('slug', $slug)->first($columns);

        if ($data === null) {
            throw new RepositoryException(__('Given id :code is invalid or permission not exist.', ['code' => $slug]), 404);
        }

        return $data;
    }

    public function findBySlugs(array $slug = [], array $relationships = [], array $columns = ['*']): Collection
    {
        return $this->model->with($relationships)->whereIn('slug', $slug)->get($columns);
    }

}
