<?php

namespace Tests;

use Totem\SamAcl\App\Model\Permission;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;

class PermissionApiTest extends ApiCrudTest
{

    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints;

    protected string $model = Permission::class;
    protected string $endpoint = 'permissions';

    protected array $withoutFields = [
        'name'
    ];

    public function testAllGrouped(): void
    {
        $model = $this->createModel(['slug' => 'app.new']);

        $response = $this->json('GET', "/api/$this->endpoint/grouped");

        $response
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'app' => [
                        '*' => [
                            'id',
                            'slug',
                            'name',
                            'description',
                        ]
                    ]
                ],
                'apiVersion'
            ])
        ->assertJsonFragment([
            'slug' => $model->slug,
            'name' => $model->name,
            'description' => $model->description,
        ]);
    }

    protected function createModel(array $attributes = []): Permission
    {
        return factory($this->model)->create($attributes);
    }

}
