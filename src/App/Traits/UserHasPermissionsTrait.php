<?php

namespace Totem\SamAcl\App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection as CollectionSupport;
use Illuminate\Support\Str;
use Totem\SamAcl\App\Model\Permission;

/**
 * @property null|Collection|Permission[] permissions
 */
trait UserHasPermissionsTrait
{

    protected ?CollectionSupport $cached_all_perms = null;

    protected ?Collection $cached_perms = null;

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }

    public function cachedAllPermissions(): CollectionSupport
    {
        if (!$this->cached_all_perms) {
            $this->cached_all_perms = $this->rolePermissions()->merge($this->cachedPermissions());
        }
        return $this->cached_all_perms;
    }

    public function cachedPermissions(): CollectionSupport
    {
        if ($this->cached_perms === null || $this->cached_perms->isEmpty()) {
            $this->cached_perms = $this->permissions()->get();
        }
        return $this->cached_perms;
    }

    public function cannot($permission): bool
    {
        return $this->cant($permission);
    }

    public function cant($permission): bool
    {
        return !$this->can($permission);
    }

    public function can($permission, bool $requireAll = false): bool
    {
        return $this->hasPermission($permission, $requireAll);
    }

    public function hasPermission($permissions, bool $requireAll = false): bool
    {
        if (!$requireAll) {
            foreach ((array)$permissions as $permission) {
                if ($this->checkPermission($permission)) {
                    return true;
                }
            }
            return false;
        }
        return $this->hasAllPermissions($permissions);
    }

    public function checkPermission($permission): bool
    {
        return $this->cachedAllPermissions()->contains(static function ($value) use ($permission) {
            return (int)$permission === $value->id || Str::is($permission, $value->slug);
        });
    }

    public function hasAllPermissions(array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (!$this->checkPermission($permission)) {
                return false;
            }
        }
        return true;
    }

    public function able($permission): bool
    {
        return $this->cachedPermissions()->contains(static function ($value) use ($permission) {
            return (int)$permission === $value->id || Str::is($permission, $value->slug);
        });
    }

    public function attachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->attachPermission($permission);
        }
    }

    public function attachPermission($permission): void
    {
        $this->checkPermission($permission) ?: $this->permissions()->attach($permission);
    }

    public function detachPermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->detachPermission($permission);
        }
    }

    public function detachPermission($permission): void
    {
        $this->permissions()->detach($permission);
    }

    public function syncPermissions($permissions): array
    {
        return $this->permissions()->sync($permissions);
    }

}
