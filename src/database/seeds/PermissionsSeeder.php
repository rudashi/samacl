<?php

namespace Totem\SamAcl\Database\Seeds;

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'slug' => 'users.view',
                'name' => 'Can View Users',
                'description' => 'Can view users',
            ],
            [
                'slug' => 'users.create',
                'name' => 'Can Create Users',
                'description' => 'Can create new users',
            ],
            [
                'slug' => 'users.edit',
                'name' => 'Can Edit Users',
                'description' => 'Can edit users',
            ],
            [
                'slug' => 'users.show',
                'name' => 'Can Show Users',
                'description' => 'Can show users',
            ],
            [
                'slug' => 'users.delete',
                'name' => 'Can Delete Users',
                'description' => 'Can delete users',
            ],
            [
                'slug' => 'users.file-upload',
                'name' => 'Can Upload User Files',
                'description' => 'Can upload and remove files from users',
            ],
            [
                'slug' => 'roles.view',
                'name' => 'Can View Roles',
                'description' => 'Can view roles',
            ],
            [
                'slug' => 'roles.modify',
                'name' => 'Can Modify Roles',
                'description' => 'Can modify roles',
            ],
            [
                'slug' => 'permissions.view',
                'name' => 'Can View Permissions',
                'description' => 'Can view permissions',
            ],
            [
                'slug' => 'permissions.modify',
                'name' => 'Can Modify Permissions',
                'description' => 'Can modify permissions',
            ],

        ];

        \Totem\SamAcl\App\Model\Permission::query()->insert($data);
    }

}
