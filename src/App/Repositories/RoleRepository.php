<?php

namespace Totem\SamAcl\App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Totem\SamAcl\App\Model\Role;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAcl\App\Repositories\Contracts\RoleRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Builder|Role model
 */
class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

    public function model(): string
    {
        return Role::class;
    }

    public function allWithUsers(array $columns = ['*']) : Collection
    {
        return $this->model->with('users')->get($columns);
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Role
    {
        if ($id === 0) {
            throw new RepositoryException( __('No role id have been given.') );
        }
        /** @var Role $data */
        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or role not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']) : Role
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function store(Request $request, int $id = 0) : Role
    {
        $role = ($id === 0) ? $this->model : $this->find($id);

        if ($id === 0) {
            $role->slug     = $request->input('slug');
        }

        $role->name         = $request->input('name');
        $role->description  = $request->input('description');

        if ($id > 0) {
            $role->syncPermissions([]);
        }

        $role->save();
        $role->attachPermissions($request->input('permissions'));

        return $role;
    }

}
