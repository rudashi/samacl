<?php

namespace Totem\SamAcl\App\Model;

use Totem\SamAcl\App\Traits\RoleTrait;
use Illuminate\Database\Eloquent\Model;
use Totem\SamAcl\App\Model\Contracts\RoleInterface;

/**
 * @property int id
 * @property string slug
 * @property string name
 * @property string description
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Collection permissions
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Eloquent\Collection users
 */
class Role extends Model implements RoleInterface
{
    use RoleTrait;

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'pivot',
        ]);

        $this->fillable([
            'slug',
            'name',
            'description',
        ]);

        parent::__construct($attributes);
    }

}
