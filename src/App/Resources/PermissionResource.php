<?php

namespace Totem\SamAcl\App\Resources;

use Totem\SamUsers\App\Resources\UserResource;
use Totem\SamCore\App\Resources\ApiResource;

/** @property \Totem\SamAcl\App\Model\Permission resource */
class PermissionResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            'users' => UserResource::collection($this->whenLoaded('users')),
        ];
    }

}
