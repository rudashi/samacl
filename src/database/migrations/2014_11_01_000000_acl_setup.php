<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class AclSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        try {
            Schema::create('permissions', static function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('slug')->unique();
                $table->string('name');
                $table->text('description')->nullable();
            });

            Schema::create('roles', static function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('slug')->unique();
                $table->string('name');
                $table->text('description')->nullable();
            });

            Schema::create('permission_role', static function (Blueprint $table) {
                $table->integer('permission_id')->unsigned();
                $table->integer('role_id')->unsigned();

                $table->foreign('permission_id')->references('id')->on('permissions')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on('roles')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['permission_id', 'role_id']);
            });

            Schema::create('role_user', static function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on('roles')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['user_id', 'role_id']);
            });

            Schema::create('permission_user', static function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('permission_id')->unsigned();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('permission_id')->references('id')->on('permissions')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['user_id', 'permission_id']);
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamAcl\Database\Seeds\RolesSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \Totem\SamAcl\Database\Seeds\PermissionsSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \Totem\SamAcl\Database\Seeds\RelationshipSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \Totem\SamAcl\Database\Seeds\UserRelationshipSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('permission_user');
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('permissions');
    }

}
