<?php

namespace Totem\SamAcl\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class RoleCollection extends ApiCollection
{

    public $collects = RoleResource::class;

}
