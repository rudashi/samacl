<?php

namespace Totem\SamAcl\Database\Seeds;

use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var null|\Totem\SamAcl\App\Model\Role $roleAdmin */
        $permissions = \Totem\SamAcl\App\Model\Permission::all();
        $roleAdmin = \Totem\SamAcl\App\Model\Role::query()->where('slug', 'admin')->first();

        if ($roleAdmin !== null) {
            foreach ($permissions as $permission) {
                $roleAdmin->attachPermission($permission);
            }
        }
    }

}
