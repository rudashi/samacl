<?php

use Illuminate\Support\Facades\Route;
use Totem\SamAcl\App\Controllers\ApiPermissionsController;
use Totem\SamAcl\App\Controllers\ApiRolesController;

Route::group(['prefix' => 'api'], static function () {

    Route::middleware(config('sam-admin.guard-api'))->group(static function () {

        Route::group(['prefix' => 'roles'], static function () {

            Route::middleware('permission:roles.modify')->group(static function () {
                Route::post('/', [ApiRolesController::class, 'create']);
                Route::put('{id}', [ApiRolesController::class, 'replace']);
                Route::delete('{id}', [ApiRolesController::class, 'destroy']);
            });

            Route::middleware('permission:roles.view')->group(static function () {
                Route::get('/', [ApiRolesController::class, 'index']);
                Route::get('{id}', [ApiRolesController::class, 'show']);
            });
        });

        Route::group(['prefix' => 'permissions'], static function () {

            Route::middleware('permission:permissions.modify')->group(static function () {
                Route::post('/', [ApiPermissionsController::class, 'create']);
                Route::put('{id}', [ApiPermissionsController::class, 'replace']);
                Route::delete('{id}', [ApiPermissionsController::class, 'destroy']);
            });

            Route::middleware('permission:permissions.view')->group(static function () {
                Route::get('/', [ApiPermissionsController::class, 'index']);
                Route::get('{id}', [ApiPermissionsController::class, 'show']);
                Route::get('grouped', [ApiPermissionsController::class, 'indexGrouped']);
            });
        });

    });

});
