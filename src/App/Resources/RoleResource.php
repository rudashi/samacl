<?php

namespace Totem\SamAcl\App\Resources;

use Totem\SamUsers\App\Resources\UserResource;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Totem\SamAcl\App\Model\Role resource
 */
class RoleResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id' => $this->resource->id,
            'slug' => $this->resource->slug,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'users' => $this->makeSimplifyCollection($request, UserResource::class, 'users'),
            'permissions' => $this->makeSimplifyCollection($request, PermissionResource::class, 'permissions'),
        ];
    }

}
