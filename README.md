Sam ACL Component
================

This package manages user roles and permissions in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require totem-it/sam-acl
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samacl.git"
    }
],
```

## Usage

### Blade directives
New directive is available for use within Blade templates.

```blade
@permission("users.view")
    This is visible for user with permission used as parameter.
@endpermission
```

It is possible to use pipe symbol as OR operator:

```blade
@permission("users.view|users.edit")
    This is visible for user with permission used as parameter.
@endpermission
```

### Middleware
You can use a middleware to filter routes and route groups by permission.

```php
Route::group(['prefix' => 'admin', 'middleware' => ['permission:admin.dashboard']], static function() {
    Route::get('/', [AdminController::class, 'dashboard'])->middleware('permission:admin.dashboard');
});
```

It is possible to use pipe symbol as OR operator:

```php
Route::get('/', [AdminController::class, 'dashboard'])->middleware('permission:users.view|users.created');
```

### Helper for seeder
You can use a special trait to insert permission directly to database. They are automatically attached to default admin role.

```php
use Totem\SamAcl\Database\PermissionTraitSeeder;

public function permissions(): array
{
    return [
        [
            'slug' => 'permission.view',
            'name' => 'Can View Permissions',
            'description' => 'Can view permissions.',
        ],
    ];
}
```

### API

Check [openapi](openapi.json) file.

Endpoints use standard filtering from `SAM-core`.

You can use query param `simplify=relationship,(...)` for simplified relationship information.
```
GET /roles?include=users&simplify=users
```

## Authors

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
