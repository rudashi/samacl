<?php

namespace Totem\SamAcl\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface PermissionRepositoryInterface extends RepositoryInterface
{

    public function allGrouped(): Collection;

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']): Permission;

    public function findById(int $id = 0, array $columns = ['*']): Permission;

    public function store(Request $request, int $id = 0): Permission;

    public function findBySlug(string $slug = null, array $relationships = [], array $columns = ['*']): Permission;

    public function findBySlugs(array $slug = [], array $relationships = [], array $columns = ['*']): Collection;

}
