<?php

namespace Totem\SamAcl;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class SamAclServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-acl';
    }

    public function boot(): void
    {
        $this->registerBladeExtensions();
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations'
        );

        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    }

    public function register(): void
    {
        $this->registerMiddlewareAlias('permission', \Totem\SamAcl\App\Middleware\PermissionMiddleware::class);

        $this->configureBinding([
            \Totem\SamAcl\App\Repositories\Contracts\RoleRepositoryInterface::class => \Totem\SamAcl\App\Repositories\RoleRepository::class,
            \Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface::class => \Totem\SamAcl\App\Repositories\PermissionRepository::class
        ]);
        $this->registerEloquentFactoriesFrom(__DIR__ . '/database/factories');
    }

    private function registerBladeExtensions(): void
    {
        Blade::directive('permission', static function ($expression) {
            return "<?php if (auth()->user()->can(explode('|', {$expression}))): ?>";
        });
        Blade::directive('endpermission', static function () {
            return '<?php endif; ?>';
        });
    }

}
