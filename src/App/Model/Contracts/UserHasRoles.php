<?php

namespace Totem\SamAcl\App\Model\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Totem\SamAcl\App\Model\Role;

interface UserHasRoles
{
    /**
     * User belongs to many roles.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany;

    /**
     * Role has many permissions.
     *
     * @return Collection
     */
    public function rolePermissions(): Collection;

    /**
     * Check if the user has a role or roles.
     *
     * @param int|string|array $role
     * @param bool $requireAll
     * @return bool
     */
    public function hasRole($role, bool $requireAll = false): bool;

    /**
     * Check if the user has all roles.
     *
     * @param array $roles
     * @return bool
     */
    public function hasAllRoles(array $roles): bool;

    /**
     * Check if the user has role.
     *
     * @param int|string $role
     * @return bool
     */
    public function checkRole($role): bool;

    /**
     * Attach role to a user.
     *
     * @param int|Role $role
     * @return void
     */
    public function attachRole($role): void;

    /**
     * Attach multiple roles to a user
     *
     * @param array $roles
     * @return void
     */
    public function attachRoles(array $roles): void;

    /**
     * Detach role from a user.
     *
     * @param int|Role $role
     * @return int
     */
    public function detachRole($role): int;

    /**
     * Detach multiple roles from a user
     *
     * @param array $roles
     * @return void
     */
    public function detachRoles(array $roles): void;

    /**
     * Sync roles for a user.
     *
     * @param array|Role[]|Collection $roles
     * @return array
     */
    public function syncRoles($roles): array;

}
