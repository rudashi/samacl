<?php

namespace Totem\SamAcl\App\Traits;

trait UserTrait
{

   use UserHasPermissionsTrait, UserHasRolesTrait;

}