<?php

namespace Totem\SamAcl\App\Traits;

use Totem\SamAcl\App\Model\Role;
use Totem\SamUsers\App\Model\User;

trait PermissionTrait
{
    public function users(): \Illuminate\Database\Eloquent\Relations\belongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function roles(): \Illuminate\Database\Eloquent\Relations\belongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    public function attachRole($role): void
    {
        $this->roles()->get()->contains($role) ?: $this->roles()->attach($role);
    }

    public function attachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function detachRole($role): int
    {
        return $this->roles()->detach($role);
    }

    public function detachRoles($roles): void
    {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function syncRoles($permissions): array
    {
        return $this->roles()->sync($permissions);
    }

}
