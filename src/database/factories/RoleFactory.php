<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamAcl\App\Model\Role::class, static function (Faker $faker) {

    return [
        'slug' => $faker->unique()->slug,
        'name' => $faker->unique()->domainWord,
        'description' => $faker->text,
    ];
});
