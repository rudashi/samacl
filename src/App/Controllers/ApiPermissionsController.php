<?php

namespace Totem\SamAcl\App\Controllers;

use Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface;
use Totem\SamAcl\App\Requests\PermissionRequest;
use Totem\SamAcl\App\Resources\PermissionCollection;
use Totem\SamAcl\App\Resources\PermissionResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiResource;

class ApiPermissionsController extends ApiController
{

    public function __construct(PermissionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(PermissionRequest $request): PermissionResource
    {
        return new PermissionResource($this->repository->store($request));
    }

    public function index(): PermissionCollection
    {
        return new PermissionCollection(
            $this->getFromRequestQuery($this->repository->all())
        );
    }

    public function show(int $id): PermissionResource
    {
        return new PermissionResource(
            $this->getFromRequestQuery($this->repository->findById($id))
        );
    }

    public function replace(int $id, PermissionRequest $request): PermissionResource
    {
        return new PermissionResource($this->repository->store($request, $id));
    }

    public function destroy(int $id): PermissionResource
    {
        return new PermissionResource($this->repository->delete($id));
    }

    public function indexGrouped(): ApiResource
    {
        return $this->response($this->repository->allGrouped());
    }

}
